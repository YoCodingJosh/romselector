﻿namespace ROMSelect
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.systemComboBox = new System.Windows.Forms.ComboBox();
            this.systemLabel = new System.Windows.Forms.Label();
            this.availableRomsLabel = new System.Windows.Forms.Label();
            this.startButton = new System.Windows.Forms.Button();
            this.romsListView = new System.Windows.Forms.ListView();
            this.myPictureBox = new System.Windows.Forms.PictureBox();
            this.copyrightLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.myPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // systemComboBox
            // 
            this.systemComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.systemComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.systemComboBox.FormattingEnabled = true;
            this.systemComboBox.Location = new System.Drawing.Point(451, 25);
            this.systemComboBox.Name = "systemComboBox";
            this.systemComboBox.Size = new System.Drawing.Size(118, 21);
            this.systemComboBox.TabIndex = 1;
            this.systemComboBox.SelectedIndexChanged += new System.EventHandler(this.systemComboBox_SelectedIndexChanged);
            // 
            // systemLabel
            // 
            this.systemLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.systemLabel.AutoSize = true;
            this.systemLabel.Location = new System.Drawing.Point(448, 9);
            this.systemLabel.Name = "systemLabel";
            this.systemLabel.Size = new System.Drawing.Size(44, 13);
            this.systemLabel.TabIndex = 2;
            this.systemLabel.Text = "System:";
            // 
            // availableRomsLabel
            // 
            this.availableRomsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.availableRomsLabel.AutoSize = true;
            this.availableRomsLabel.Location = new System.Drawing.Point(12, 9);
            this.availableRomsLabel.Name = "availableRomsLabel";
            this.availableRomsLabel.Size = new System.Drawing.Size(86, 13);
            this.availableRomsLabel.TabIndex = 3;
            this.availableRomsLabel.Text = "Available ROMs:";
            // 
            // startButton
            // 
            this.startButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.startButton.Enabled = false;
            this.startButton.ForeColor = System.Drawing.Color.Green;
            this.startButton.Location = new System.Drawing.Point(451, 52);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(118, 25);
            this.startButton.TabIndex = 4;
            this.startButton.Text = "Start Game!";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // romsListView
            // 
            this.romsListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.romsListView.Location = new System.Drawing.Point(12, 25);
            this.romsListView.MultiSelect = false;
            this.romsListView.Name = "romsListView";
            this.romsListView.Size = new System.Drawing.Size(433, 303);
            this.romsListView.TabIndex = 5;
            this.romsListView.UseCompatibleStateImageBehavior = false;
            this.romsListView.View = System.Windows.Forms.View.List;
            this.romsListView.ItemActivate += new System.EventHandler(this.romsListView_ItemActivate);
            this.romsListView.SelectedIndexChanged += new System.EventHandler(this.romsListView_SelectedIndexChanged);
            // 
            // myPictureBox
            // 
            this.myPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.myPictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.myPictureBox.Image = global::ROMSelect.Properties.Resources.troll_troll_wario_s;
            this.myPictureBox.Location = new System.Drawing.Point(451, 81);
            this.myPictureBox.Name = "myPictureBox";
            this.myPictureBox.Size = new System.Drawing.Size(118, 87);
            this.myPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.myPictureBox.TabIndex = 6;
            this.myPictureBox.TabStop = false;
            this.myPictureBox.Click += new System.EventHandler(this.myPictureBox_Click);
            // 
            // copyrightLabel
            // 
            this.copyrightLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.copyrightLabel.AutoSize = true;
            this.copyrightLabel.Enabled = false;
            this.copyrightLabel.Location = new System.Drawing.Point(451, 302);
            this.copyrightLabel.Name = "copyrightLabel";
            this.copyrightLabel.Size = new System.Drawing.Size(117, 26);
            this.copyrightLabel.TabIndex = 7;
            this.copyrightLabel.Text = "Copyright © 2012-2013\r\nJoshua Kennedy";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(581, 340);
            this.Controls.Add(this.copyrightLabel);
            this.Controls.Add(this.myPictureBox);
            this.Controls.Add(this.romsListView);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.availableRomsLabel);
            this.Controls.Add(this.systemLabel);
            this.Controls.Add(this.systemComboBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select a ROM!";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.myPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox systemComboBox;
        private System.Windows.Forms.Label systemLabel;
        private System.Windows.Forms.Label availableRomsLabel;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.ListView romsListView;
        private System.Windows.Forms.PictureBox myPictureBox;
        private System.Windows.Forms.Label copyrightLabel;
    }
}

