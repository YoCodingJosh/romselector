﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Windows.Forms;

namespace ROMSelect
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Debug.WriteLine("ROM Selector Started!");

            foreach (string directory in Directory.GetDirectories("./ROMs"))
            {
                string simple = directory.Remove(0, directory.IndexOf("\\") + 1);
                systemComboBox.Items.Add(simple);
            }

            systemComboBox.SelectedIndex = 0;
        }

        private void systemComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            romsListView.Items.Clear();

            startButton.Enabled = false;

            Debug.WriteLine("Fetching ROMs in " + "./ROMs/" + systemComboBox.Items[systemComboBox.SelectedIndex].ToString() + "...");

            foreach (string file in Directory.GetFiles("./ROMs/" + systemComboBox.Items[systemComboBox.SelectedIndex].ToString()))
            {
                if (file.EndsWith(".zip"))
                    romsListView.Items.Add(file);
            }
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            StartROM();
        }

        private void StartROM()
        {
            if (romsListView.SelectedItems.Count <= 0)
            {
                MessageBox.Show("Please select a ROM to start!", "Error - ROM Selector");
                return;
            }

            string trimmed = romsListView.SelectedItems[0].ToString().Remove(0, 15);
            trimmed = trimmed.Remove(trimmed.IndexOf('}'));

            if (systemComboBox.Items[systemComboBox.SelectedIndex].ToString().Equals("Game Boy") || systemComboBox.Items[systemComboBox.SelectedIndex].ToString().Equals("Game Boy Color") || systemComboBox.Items[systemComboBox.SelectedIndex].ToString().Equals("Game Boy Advance"))
            {
                Process p = new Process();
                p.StartInfo.FileName = Environment.CurrentDirectory + "./EMUs/VBA-M/VisualBoyAdvance-M.exe";
                p.StartInfo.Arguments = "\"" + trimmed + "\"";
                p.Start();
                this.Hide();
                p.WaitForExit();
                this.Show();
            }
            else if (systemComboBox.Items[systemComboBox.SelectedIndex].ToString().Equals("Nintendo 64"))
            {
                Process p = new Process();
                p.StartInfo.FileName = Environment.CurrentDirectory + "./EMUs/PJ64/Project64.exe";
                p.StartInfo.Arguments = trimmed;
                p.Start();
                this.Hide();
                p.WaitForExit();
                this.Show();
            }

            else if (systemComboBox.Items[systemComboBox.SelectedIndex].ToString().Equals("SNES"))
            {
                Process p = new Process();
                p.StartInfo.FileName = Environment.CurrentDirectory + "./EMUs/zsnesw151/zsnesw.exe";
                p.StartInfo.Arguments = "\"" + trimmed + "\"";
                p.Start();
                this.Hide();
                p.WaitForExit();
                this.Show();
            }
        }

        private void romsListView_ItemActivate(object sender, EventArgs e)
        {
            StartROM();
        }

        private void romsListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (romsListView.SelectedItems != null || romsListView.SelectedItems.Count > 0)
                startButton.Enabled = true;
            else if (romsListView.SelectedIndices[0] == -1)
                startButton.Enabled = false;
        }

        private void myPictureBox_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Made with love, from Josh!", "To " + Environment.UserName, MessageBoxButtons.OK, MessageBoxIcon.None);
        }
    }
}
