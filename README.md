ROM Selector
===========

I made this for my autistic brother who loves playing ROMs.

This program goes through the ROMs directory where the executable is located and displays the ROMs for each system.

Todo's
------
- Rather than hard-coding emulator info, use configuration files and make it more modular.

The Example directory is the directory architecture that the program requires.

###### You're probably wondering why there's a picture of Wario in the program. My brother's favorite Nintendo character is Wario.