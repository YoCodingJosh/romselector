EMUs
---
PJ64 is for the Project 64 N64 emulator
VBA-M is for the Visual Boy Advance M GB/GBC/GBA emulator
zsnesw151 is for the ZSNES SNES emulator

ROMs
---
Where you put your ROMs at. Every folder supports Zip files!